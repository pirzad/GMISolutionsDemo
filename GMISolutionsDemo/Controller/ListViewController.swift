//
//  ViewController.swift
//  GMISolutionsDemo
//
//  Created by TSIT iMac on 4/29/18.
//  Copyright © 2018 TSIT iMac. All rights reserved.
//

import UIKit

class ListViewController: UIViewController {

    @IBOutlet weak var listTableView: UITableView!
    var model: [Fact] = []
    var refreshControl = UIRefreshControl()
    let cellIdentifier: String = "ListTableViewCell"
    let pullToRefreshStr = "Pull to refresh"
    let estimatedRowHeight: CGFloat = 80
    let webServiceAddressStr = "https://dl.dropboxusercontent.com/s/2iodh4vg0eortkl/facts.json"
    override func viewDidLoad() {
        super.viewDidLoad()
        // Set tableview row height Automatic
        self.listTableView.rowHeight = UITableViewAutomaticDimension
        self.listTableView.estimatedRowHeight = self.estimatedRowHeight
        // Register nib
        let listTableViewNib: UINib = UINib(nibName: self.cellIdentifier, bundle: nil)
        self.listTableView.register(listTableViewNib, forCellReuseIdentifier: self.cellIdentifier)
        self.listTableView.delegate = self
        self.listTableView.dataSource = self
        self.listTableView.isHidden = true
        // Add Pull to refresh
        self.refreshControl.attributedTitle = NSAttributedString(string: self.pullToRefreshStr)
        self.refreshControl.addTarget(self, action: #selector(self.refresh(sender:)), for: UIControlEvents.valueChanged)
        self.listTableView.addSubview(self.refreshControl)
        // Updating your data here...
        self.refreshData()
    }
    @objc func refresh(sender: UIRefreshControl) {
        // Updating your data here...
        self.refreshData()
    }
    @objc func refreshData() {
        // Get data from server and reload tableview
        let indicatorView = UIActivityIndicatorView(activityIndicatorStyle: .gray)
        indicatorView.hidesWhenStopped = false
        indicatorView.startAnimating()
        self.navigationController?.topViewController?.navigationItem.titleView = indicatorView
        self.getData(urlStr: self.webServiceAddressStr) { (titleStr, jsonModelArray) in
            DispatchQueue.main.async {
                self.listTableView.isHidden = false
                if self.refreshControl.isRefreshing == true {
                    self.refreshControl.endRefreshing()
                }
                if jsonModelArray.count > 0 {
                    // set modelData
                    self.model = jsonModelArray
                    self.listTableView.reloadData()
                }
                // Remove IndicatorView from titleView
                indicatorView.removeFromSuperview()
                self.navigationController?.topViewController?.navigationItem.titleView = nil
                // set title navigationBar
                self.navigationController?.topViewController?.title = titleStr
            }
        }
    }
    func getData(urlStr: String, completionHandler: @escaping ((String, [Fact]) -> Void)) {
        let urlString = URL(string: urlStr)
        let dataModel: [Fact] = []
        if let url = urlString {
            let task = URLSession.shared.dataTask(with: url) { (data, _, error) in
                if error != nil {
                    if let errorStr = error?.localizedDescription {
                        print(errorStr)
                        completionHandler(errorStr, dataModel)
                    }
                } else {
                    if let data = data {
                        // validate json
                        let dataString = String(data: data, encoding: String.Encoding.ascii) as String!
                        let resultData = dataString?.data(using: String.Encoding.utf8)
                        do {
                            if let resultData = resultData {
                                let jsonDic = try JSONSerialization.jsonObject(with: resultData,
                                                                               options: []) as? NSDictionary
                                if let rows = jsonDic?.value(forKey: "rows") as? NSArray {
                                    let jsonData = try JSONSerialization.data(withJSONObject: rows,
                                                                              options:
                                        JSONSerialization.WritingOptions.prettyPrinted)
                                    let jsonDecoder = JSONDecoder()
                                    // Parse facts
                                    let facts = try jsonDecoder.decode([Fact].self, from: jsonData)
                                    if let title = jsonDic?.value(forKey: "title") as? String {
                                        // Parse title navigation bar
                                        completionHandler(title, facts)
                                    }
                                }
                            }
                        } catch let err {
                            // handle error
                            print("Error", err.localizedDescription)
                            completionHandler(err.localizedDescription, dataModel)
                        }
                    } else {
                        completionHandler("", dataModel)
                    }
                }
            }
            task.resume()
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

}
