//
//  ListTableViewDelegates.swift
//  GMISolutionsDemo
//
//  Created by TSIT iMac on 4/29/18.
//  Copyright © 2018 TSIT iMac. All rights reserved.
//

import UIKit

extension ListViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.model.count
    }
}

extension ListViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: self.cellIdentifier) as? ListTableViewCell {
            // Get modelData
            let fact = self.model[indexPath.row]
            cell.selectionStyle = .none
            // Set Content
            cell.descriptionLabel.text = fact.description
            cell.titleLabel.text = fact.title
            cell.avatarImageView.downloadImageFrom(link: fact.imageHref)
            return cell
        } else {
            return UITableViewCell()
        }
    }
}
