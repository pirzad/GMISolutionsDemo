//
//  ListTableViewCell.swift
//  GMISolutionsDemo
//
//  Created by TSIT iMac on 4/29/18.
//  Copyright © 2018 TSIT iMac. All rights reserved.
//

import UIKit

class ListTableViewCell: UITableViewCell {

    @IBOutlet weak var avatarImageView: UIImageView!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        self.autoLayoutDescriptionLabel()
        self.autoLayoutTitleLabel()
        self.autoLayoutAvatarImageView()
    }
    func autoLayoutDescriptionLabel() {
        self.descriptionLabel.translatesAutoresizingMaskIntoConstraints = false
        let descriptionLabelLeadingConstraint = NSLayoutConstraint(item: self.descriptionLabel,
                                                                   attribute: .leading, relatedBy: .equal,
                                                                   toItem: self.contentView,
                                                                   attribute: .leading,
                                                                   multiplier: 1,
                                                                   constant: 100)
        let descriptionLabelTrailingConstraint = NSLayoutConstraint(item: self.descriptionLabel,
                                                                    attribute: .trailing, relatedBy: .equal,
                                                                    toItem: self.contentView,
                                                                    attribute: .trailing,
                                                                    multiplier: 1,
                                                                    constant: -10)
        let descriptionLabelTopConstraint = NSLayoutConstraint(item: self.descriptionLabel,
                                                               attribute: .top, relatedBy: .equal,
                                                               toItem: self.contentView,
                                                               attribute: .top,
                                                               multiplier: 1,
                                                               constant: 50)
        let descriptionLabelBottomConstraint = NSLayoutConstraint(item: self.descriptionLabel,
                                                                  attribute: .bottom, relatedBy: .equal,
                                                                  toItem: self.contentView,
                                                                  attribute: .bottom,
                                                                  multiplier: 1,
                                                                  constant: -10)
        NSLayoutConstraint.activate([descriptionLabelLeadingConstraint,
                                     descriptionLabelTrailingConstraint,
                                     descriptionLabelTopConstraint,
                                     descriptionLabelBottomConstraint])
    }
    func autoLayoutTitleLabel() {
        self.titleLabel.translatesAutoresizingMaskIntoConstraints = false
        let titleLabelLeadingConstraint = NSLayoutConstraint(item: self.titleLabel,
                                                                   attribute: .leading, relatedBy: .equal,
                                                                   toItem: self.contentView,
                                                                   attribute: .leading,
                                                                   multiplier: 1,
                                                                   constant: 100)
        let titleLabelTrailingConstraint = NSLayoutConstraint(item: self.titleLabel,
                                                                    attribute: .trailing, relatedBy: .equal,
                                                                    toItem: self.contentView,
                                                                    attribute: .trailing,
                                                                    multiplier: 1,
                                                                    constant: -10)
        let titleLabelTopConstraint = NSLayoutConstraint(item: self.titleLabel,
                                                               attribute: .top, relatedBy: .equal,
                                                               toItem: self.contentView,
                                                               attribute: .top,
                                                               multiplier: 1,
                                                               constant: 20)

        NSLayoutConstraint.activate([titleLabelLeadingConstraint,
                                     titleLabelTrailingConstraint,
                                     titleLabelTopConstraint ])
    }
    func autoLayoutAvatarImageView() {
        self.avatarImageView.translatesAutoresizingMaskIntoConstraints = false
        let avatarImageViewLeadingConstraint = NSLayoutConstraint(item: self.avatarImageView,
                                                             attribute: .leading, relatedBy: .equal,
                                                             toItem: self.contentView,
                                                             attribute: .leading,
                                                             multiplier: 1,
                                                             constant: 10)

        let avatarImageViewTopConstraint = NSLayoutConstraint(item: self.avatarImageView,
                                                         attribute: .centerY, relatedBy: .equal,
                                                         toItem: self.contentView,
                                                         attribute: .centerY,
                                                         multiplier: 1,
                                                         constant: 0)
        let avatarImageViewWidthConstraint = NSLayoutConstraint(item: self.avatarImageView,
                                                              attribute: .width, relatedBy: .equal,
                                                              toItem: nil,
                                                              attribute: .width,
                                                              multiplier: 1,
                                                              constant: 50)
        let avatarImageViewHeightConstraint = NSLayoutConstraint(item: self.avatarImageView,
                                                                attribute: .height, relatedBy: .equal,
                                                                toItem: nil,
                                                                attribute: .height,
                                                                multiplier: 1,
                                                                constant: 50)
        NSLayoutConstraint.activate([avatarImageViewLeadingConstraint,
                                     avatarImageViewTopConstraint,
                                     avatarImageViewWidthConstraint,
                                     avatarImageViewHeightConstraint ])
    }
    override func prepareForReuse() {
        super.prepareForReuse()
        self.avatarImageView.image = nil
    }
}
