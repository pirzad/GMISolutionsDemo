//
//  UIImageView+Extension.swift
//  GMISolutionsDemo
//
//  Created by TSIT iMac on 4/29/18.
//  Copyright © 2018 TSIT iMac. All rights reserved.
//

import UIKit

extension UIImageView {
    func downloadImageFrom(link: String) {
        let image = UIImage.init(named: "placeholder-image")!
        self.image = image
        guard let url = URL(string: link) else {
            return
        }
        let imageCache = ImageDownloader.shared.getImageFromCache(url: url)
        if imageCache != nil {
            self.image = imageCache
        } else {
            URLSession.shared.dataTask(with: url) { data, response, error in
                guard
                    let httpURLResponse = response as? HTTPURLResponse, httpURLResponse.statusCode == 200,
                    let mimeType = response?.mimeType, mimeType.hasPrefix("image"),
                    let data = data, error == nil,
                    let image = UIImage(data: data)
                    else { return }
                DispatchQueue.main.async {
                    self.image = image
                    ImageDownloader.shared.setImageToCache(url: url, image: image)
                }
                }.resume()
        }
    }
}
