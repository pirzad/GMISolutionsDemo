//
//  ImageDownloader.swift
//  GMISolutionsDemo
//
//  Created by TSIT iMac on 4/30/18.
//  Copyright © 2018 TSIT iMac. All rights reserved.
//

import UIKit

class ImageDownloader: NSObject {
    static let shared = ImageDownloader()
    let imageCache = NSCache<NSString, UIImage>()
    func getImageFromCache(url: URL) -> UIImage? {
        if let cachedImage = self.imageCache.object(forKey: url.absoluteString as NSString) {
            return cachedImage
        } else {
            return nil
        }
    }
    func setImageToCache(url: URL, image: UIImage) {
        self.imageCache.setObject(image, forKey: url.absoluteString as NSString)
    }
}
