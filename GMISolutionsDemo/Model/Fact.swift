//
//  JsonModel.swift
//  GMISolutionsDemo
//
//  Created by TSIT iMac on 4/29/18.
//  Copyright © 2018 TSIT iMac. All rights reserved.
//

import UIKit

struct Fact {
    var title: String
    var description: String
    var imageHref: String

    enum CodingKeys: String, CodingKey {
        case title
        case description
        case imageHref
    }
}

extension Fact: Decodable {
    init(from decoder: Decoder) throws {
        // Check nil data and fill struct
        let values = try decoder.container(keyedBy: CodingKeys.self)
        if let titleStr = try values.decode(String?.self, forKey: .title) {
            self.title = titleStr
        } else {
            self.title = "title is nil"
        }
        if let desc = try values.decode(String?.self, forKey: .description) {
            self.description = desc
        } else {
            self.description = "description is nil"
        }
        if let image = try values.decode(String?.self, forKey: .imageHref) {
            self.imageHref = image
        } else {
            self.imageHref = ""
        }
    }
}
